test_graph <- matrix(c(0, 5, 6, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 10,5, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 4, 1, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 9, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 20,10,0, 0,
                       0, 0, 0, 0, 2, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0,5,
                       0, 0, 0, 0, 0, 0, 5, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                       nrow=10, ncol=10, byrow=TRUE)

#Убираем взвешанные ребра
rmw_weight <- function(graph){
  n <- ncol(graph)
  graph[((graph == 1) + (graph == 0)) == 0] <- 1
  return(graph)
}

#Делаем из дуг ребра
rmw_directions <- function(graph){
  n <- ncol(graph)
  for(i in 1:(n-1)){
    for(j in (i+1):n){
      if(graph[i,j] != graph[j,i]){
        graph[i,j] <- graph[j,i] <- 1
      }
    }
  }
  return(graph)
}

#Убираем петли
rmw_loops <- function(graph){
  for(i in 1:ncol(graph)){
    if(graph[i,i] != 0){
      graph[i,i] <- 0
    }
  }
  return(graph)
}

#Количество вершин
vertex_amnt <- function(graph){
  s <- 0
  for(i in 1:nrow(graph)) {
    if(sum(graph[i,]) != 0) {
      s <- s + 1
    }
  }
  return(s) 
}

#Поиск ребра
find_edge <- function(graph, v){
  n <- ncol(graph)
  s <- 0
  for(i in 1:(n - 1)){
    for(j in (i + 1):n){
      s <- s + graph[i, j]
      if(s >= v){
        return(c(i, j))
      }
    }
  }
}

#Стягиваем ребра
contract <- function(graph, v1, v2){
  graph[v1, ] <- graph[v1, ] + graph[v2, ]
  graph[, v1] <- graph[v1, ]
  graph[v2, ] <- 0
  graph[, v2] <- 0
  graph <- rmw_loops(graph)
  return(graph)
}

#Разрез графа
cut_graph <- function(graph){
  graph <- rmw_loops(rmw_directions(rmw_weight(graph)))
  vis_graph <- graph.adjacency(graph, mode = "undirected")
  plot.igraph(vis_graph)
  while(vertex_amnt(graph) > 2){
    edge_amnt <- sum(graph)/2
    v <- sample(1:edge_amnt, 1)
    rng_edge <- find_edge(graph, v)
    graph <- contract(graph, rng_edge[1], rng_edge[2])
    vis_graph <- graph.adjacency(graph, mode = "undirected")
    plot.igraph(vis_graph)
  }
  vis_graph <- graph.adjacency(graph, mode = "undirected")
  plot.igraph(vis_graph)
  return(sum(graph)/2)
}

cut_graph(test_graph)

#Минимальный разрез графа
min_cut_graph <- function(graph){
  n <- ncol(graph)
  min_cut <- Inf
  for(i in 1:100){
    random_cut <- cut_graph(graph)
    if(random_cut < min_cut){
      min_cut <- random_cut
    }
    if(min_cut == 1){
      return(min_cut)
    }
  }
  return(min_cut)
}
