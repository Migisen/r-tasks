find_min <- function(f, a, eps, h, graph = "graph"){
  print(f)
  #Проверки
  if(eps <= 0){
    stop("Эпсилон - не положительное число")
  }
  if(h <= 0){
    stop("Не верно указан h")
  }
  
  
  #График
  if(graph != "no graph"){
    x <- c(seq(-10, 10, len=50))
    y <- x
    z <- outer(x, y, FUN = f)
    par(mfcol = c(1, 2))
    persp(x, y, z, phi = 30)
    contour(x, y, z)
    points(a[1], a[2], pch = 20)
  }
  
  
  #Поиск минимума
  ex <- c(1, 0)
  ey <- c(0, 1)
  a1 <- a
  while(h >= eps){
    if(f(a[1], a[2]) > f((a - h*ex)[1], (a - h*ex)[2])){
      a1 <- a - h*ex
    } else if(f(a[1], a[2]) > f((a + h*ex)[1], (a + h*ex)[2])){
      a1 <- a + h*ex
    } else if(f(a[1], a[2]) > f((a - h*ey)[1], (a - h*ey)[2])){
      a1 <- a - h*ey
    } else if(f(a[1], a[2]) > f((a + h*ey)[1], (a + h*ey)[2])){
      a1 <- a + h*ey
    }
    
    if(all(a1 == a)){
      h <- h/2
    } else{
      a2<- 2*a1-a
      points(a1[1], a1[2], col="blue", pch=19)
      segments(a[1], a[2], a1[1], a1[2], col="blue")
      points(a2[1], a2[2], col="red")
      while(f(a2[1], a2[2]) < f(a1[1], a1[2])) {
        a<- a1
        a1<- a2
        a2<- 2*a1-a
        points(a1[1], a1[2], col="blue", pch=19)
        segments(a[1], a[2], a1[1], a1[2], col="blue")
        points(a2[1], a2[2], col="red")
      }
      a <- a1
    }
  }
  points(a[1], a[2], pch=18)
  return(list(a, f(a[1], a[2])))
}


#Примеры
test1 <- function(x, y){x^2 + y^2}
find_min(test1, c(9, 8), eps = 0.1, h = 1)


